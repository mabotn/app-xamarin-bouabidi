﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace XamarinApp.Views
{
	public partial class Login : ContentPage
	{
		public Login ()
		{
			InitializeComponent ();
            LoginButton.GestureRecognizers.Add(new TapGestureRecognizer() {
                Command = new Command(() =>
                {
                    Navigation.PushModalAsync(new Views.Interests());
                }),
                NumberOfTapsRequired = 1
            });
		}
	}
}
